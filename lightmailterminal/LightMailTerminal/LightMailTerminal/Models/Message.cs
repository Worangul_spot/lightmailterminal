﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightMailTerminal.Models
{
    class Message
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public List<string> Receivers { get; set; }
        public Sender Sender { get; set; }
        public bool IsChoosen { get; set; }
        public string SaveFile { get; set; }
    }
}
