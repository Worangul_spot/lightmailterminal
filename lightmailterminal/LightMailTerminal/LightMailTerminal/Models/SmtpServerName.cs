﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightMailTerminal.Models
{
    class SmtpServerName
    {
        public string Name { get; set; }
        public int Port { get; set; }
        public bool IsChoosen { get; set; }
    }
}
