﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightMailTerminal.Models
{
    class Command
    {
        public string CommandHead { get; set; }
        public string Object { get; set; }
        public string CommandBody { get; set; }
        private void ChangeDirectory()
        {}
        private void MakeMessage()
        {}
        private void DeleteMessage()
        {}
        private void Send()
        {}
        private void Edit()
        {}
    }
}
