﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightMailTerminal.Models
{
    class Sender
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsChoosen { get; set; }
    }
}
