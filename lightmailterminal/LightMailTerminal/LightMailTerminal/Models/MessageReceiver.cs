﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightMailTerminal.Models
{
    class MessageReceiver
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsChoosen { get; set; }
    }
}
