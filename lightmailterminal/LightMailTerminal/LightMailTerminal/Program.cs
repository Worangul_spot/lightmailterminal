﻿using System;
using LightMailTerminal.Controlers;
using LightMailTerminal.Models;
using System.IO;
using System.Collections;
using System.Collections.Generic;


namespace LightMailTerminal
{
    class Program
    {
        static string actualCommand = "";
        static bool programOpen = true;
        static Sender mailSender= new Sender()
        {
            Email="",
            Password=""
        };

        static public Message message= new Message()
        {
            Body = "",
            Subject = "",
            Sender = mailSender,
            Receivers = new List<string>()
        };


        static string messageName="nothing";

        static void Main(string[] args)
        {
            CommandController MainController = new CommandController();


            Console.WriteLine("Light Mail Terminal is now running");
            while (programOpen == true)
            {
                Console.WriteLine();
                Console.WriteLine("Pointing at: "+messageName);
                Console.WriteLine("command input: ");
                switch(CommandFilter.VerifyCommand(Console.ReadLine()))
                {
                    case "send": MainController.SendMail(Program.message);
                      //  CommandController.sendMail(Messages.Find(x=>x.IsChoosen==true), mailSenders.Find(x=>x.IsChoosen==true));
                        break;
                    case "pwd": MainController.PrintWorkingDirectory();
                        //Console.WriteLine(Messages.Find(x=>x.IsChoosen==true).Body);
                        //Messages.Find(x=>x.IsChoosen==true).Body+= Console.Read();
                        break;
                    case "ls": MainController.ListFiles();
                        break;
                    case "exit": programOpen=false;
                        break;
                    case "cd": MainController.ChangeDirectory(CommandFilter.pointer);
                        break;
                    case "cd -abs": MainController.ChangeAbsoluteDirectory(CommandFilter.pointer);
                        break;
                    case "edit subject":
                        MainController.Edit("subject");
                        break;
                    case "edit sender":
                        MainController.Edit( "sender");
                        break;
                    case "edit body":
                        MainController.Edit( "body");
                        break;
                    case "add receivers":
                        MainController.Edit("add-receivers");
                        break;
                    case "edit credentials":
                        MainController.Edit("credentials");
                        break;
                    case "show ports":
                        MainController.Show("ports");
                        break;
                    case "show message":
                        MainController.Show("message");
                        break;
                    case "save":
                        MainController.Save(Program.message);
                        break;
                    case "get mail":
                        ImapController.GetMessages(Program.message);
                        break;
                    default :
                    Console.WriteLine("MailTerminal: I don't get it. What do you mean?? :/");
                        break;
                }
            }
        }
    }
}
