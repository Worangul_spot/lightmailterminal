﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
//using System.Net.Mail;
using System.Linq.Expressions;
using System.Xml;
using System.IO;
using LightMailTerminal.Models;
using System.Net;
using System.Linq.Expressions;
using MailKit;
using MailKit.Security;
using MimeKit;
using MailKit.Net.Smtp;

namespace LightMailTerminal.Controlers
{
    class CommandController
    {
        static string CurrentDirectory;
        CommandController commandController()
        {
            return new CommandController();
        }
        //public enum Command {send,editMessage, editReceiver, editSubject, editPort, showMessages, showSenders, showReceivers, chooseMessage, chooseReceivers, chooseSender, addMessage, addReceiver, addSender  }

        public List<SmtpServerName> smtpServersNames = new List<SmtpServerName>
        {
            new SmtpServerName {Name = "smtp.gmail.com", Port = 587, IsChoosen=true },
            new SmtpServerName {Name = "smtp.live.com", Port = 587,IsChoosen=false },
            new SmtpServerName {Name = "smtp.office365.com", Port = 587, IsChoosen=false },
            new SmtpServerName {Name = "smtp.mail.yahoo.com", Port = 465, IsChoosen=false },
            new SmtpServerName {Name = "smtp.o2.ie", Port = 25, IsChoosen=false},
            new SmtpServerName {Name = "smtp.live.com", Port = 465,IsChoosen=false  }
        };
        public List<MessageReceiver> MReceivers = new List<MessageReceiver> { };
        public int ActualMailPort;
        protected string selText;

        internal void PointAt(string fileName)
        {
          try
          {
            if(Directory.Exists(Directory.GetCurrentDirectory()+"/"+fileName))
            {
              using(XmlReader reader = XmlReader.Create(fileName))
              {
                if(reader.IsStartElement())
                {
                  Program.message.Subject = reader["Subject"];
                  Program.message.Body = reader["Body"];
                  Program.message.Sender.Email = reader["Email"];
                  //Program.message.Receivers =
                }
              }
            }
            else
            {
              Console.WriteLine("There is no such file in current directory!");
            }
          }
          catch(Exception e)
          {
            Console.WriteLine("There is a problem: {0}",e.Message);
          }
        }

        internal void Save(Message message)
        {
            XmlWriterSettings xwSetting = new XmlWriterSettings();

            xwSetting.Indent = true;
            xwSetting.IndentChars = "\t";
            xwSetting.ConformanceLevel = ConformanceLevel.Auto;

           string fileName = "" + message.Subject + ".xml";
           XmlWriter writer = XmlWriter.Create(fileName, xwSetting);
           if(
              message.Subject == "" && message.Body == "" &&
              message.Receivers.Count < 1 && message.Sender.Email == "" &&
              message.Sender.Password == ""
              )
           {
             Console.WriteLine("Your message is totally blank! \n\r add anything to your message.");
             goto Finish;
           }

           writer.WriteStartDocument();
            writer.WriteStartElement("MailMessage");
              writer.WriteStartElement("Subject");
                writer.WriteString(message.Subject);
              writer.WriteEndElement();
              writer.WriteStartElement("Body");
                writer.WriteString(message.Body);
              writer.WriteEndElement();
              writer.WriteStartElement("Sender");
                writer.WriteStartElement("Email");
                  writer.WriteString(message.Sender.Email);
                writer.WriteEndElement();
                writer.WriteStartElement("Password");
                  writer.WriteString(message.Sender.Password);
                writer.WriteEndElement();
              writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
            Finish:;
        }


        internal void SendMail(Message message)
        {
            try
            {
              var senders = new List<string>();
              var receivers = new List<string>();
              senders.Add(message.Sender.Email);
              foreach(string receiver in message.Receivers)
              {
                receivers.Add(receiver);
              }
              var sendedMessage = new MimeMessage();
              sendedMessage.Body = message.Body;
                using(var client =  new SmtpClient())
                {

                  client.Connect(smtpServersNames.Find(x => x.IsChoosen==true).Name,
                  smtpServersNames.Find(x => x.IsChoosen==true).Port, SecureSocketOptions.SslOnConnect);
                  if(message.Sender!=null || message.Sender.Email != "" || message.Sender.Password != "")
                  {
                    client.Authenticate(message.Sender.Email, message.Sender.Password);
                  }
                  else
                  {
                    Console.WriteLine("Write Senders Email:");
                    message.Sender.Email = Console.ReadLine();
                    Console.WriteLine("Write sender password:");
                    ConsoleKeyInfo keyChar;
                    string passwordInput = "";
                    do
                    {
                      if(keyChar.Key != ConsoleKey.Backspace)
                      {
                        passwordInput += keyChar.Key.ToString();
                        Console.Write("*");
                      }
                      else
                      {
                        passwordInput = passwordInput.Substring(0,passwordInput.Length-1);
                        Console.Write("\b");
                      }
                      continue;
                    }
                    while(keyChar.Key != ConsoleKey.Enter);
                    message.Sender.Password = passwordInput;
                    client.Authenticate(message.Sender.Email, message.Sender.Password);
                    break;
                  }
                  client.Send(sendedMessage);
                }


                //statusStrip.Text = string.Format("Wiadomości zostały wysłane w ilości: {0} !", MReceivers.Count);
                Console.WriteLine(string.Format("Wiadomości zostały wysłane w ilości: {0} !", MReceivers.Count));
                //MessageBox.Show(string.Format("Wiadomości zostały wysłane w ilości: {0} !", MReceivers.Count.ToString()), "Komunikat o wysyłce.");
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                //statusStrip.Text = string.Format(" :( {0}", ex.ToString());
                Console.WriteLine(ex.ToString());
            }
        }
        internal void ChangeDirectory(string directory)
        {
          int i;
            try
            {
                if(directory.Contains("/"))
                {
                  string stepDirectory;
                  List<string> DirectorySteps = new List<string>();
                  DirectorySteps = CommandFilter.ExtractDirectorySteps(directory);
                  foreach(string Step in DirectorySteps)
                  {
                    stepDirectory= Directory.GetCurrentDirectory()+"/"+Step;
                    if(Directory.Exists(stepDirectory))
                    {
                      Directory.SetCurrentDirectory(stepDirectory);
                    }
                    else if(Step=="..")
                    {
                      string actualDirectory;
                      string superDirectory;
                      string sChar;
                      actualDirectory = Directory.GetCurrentDirectory();
                      for(i=actualDirectory.Length-1;i==0;i--)
                      {
                        if(actualDirectory.Substring(i,i+1)=="/")
                        Directory.SetCurrentDirectory(actualDirectory.Substring(0,i));
                      }
                    }
                    else
                    {
                      Console.WriteLine("Wrong path. Write it correctly!");
                      break;
                    }
                  }
                }
                else
                {
                  string newDirectory = Directory.GetCurrentDirectory()+"/"+directory;
                  if(Directory.Exists(newDirectory))
                  {
                    Directory.SetCurrentDirectory(newDirectory);
                  }
                  else
                  {
                    Console.WriteLine("I can't find that directory. Try something else.");
                  }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        internal void ChangeAbsoluteDirectory(string pointer)
        {
          //Directory.CurrentDirectory();
        }


        internal void Send()
        {
          Console.WriteLine("Message is sended");
            //  CommandController.sendMail(Messages.Find(x=>x.IsChoosen==true), mailSenders.Find(x=>x.IsChoosen==true));
        }

        internal void Edit(string property)
        {
          switch(property)
          {
            case "subject":
              Console.Write("Choose a title:");
              Program.message.Subject= Console.ReadLine();
              break;
            case "body":
              Console.Write("Edit message content:");
              Program.message.Body = Console.ReadLine();
              break;
            case "add-receivers":
              Program.message.Receivers.Add(Console.ReadLine());
              break;
            case "rm-receivers":
              this.RemoveReceiversFromMessage(Program.message);
              break;
            case "sender":
              Console.WriteLine("Write a Sender email:");
              Program.message.Sender.Email = Console.ReadLine();
              string mailBoxProvider;
              if(Program.message.Sender.Email.IndexOf("@") == Program.message.Sender.Email.LastIndexOf("@"))
              {
                mailBoxProvider = Program.message.Sender.Email.Substring(Program.message.Sender.Email.IndexOf("@"));
              }
              else
              {
                mailBoxProvider = "";
              }
              switch(mailBoxProvider)
              {
                //smtp.gmail.com
                    case "gmail.com":
                      foreach(SmtpServerName name in smtpServersNames)
                      {
                        if(name.Name == "smtp.gmail.com")
                        {
                          name.IsChoosen = true;
                        }
                        else
                        {
                          name.IsChoosen = false;
                        }
                      }
                      break;
                      //smtp.live.com
                      case "live.com":
                        foreach(SmtpServerName name in smtpServersNames)
                        {
                          if(name.Name == "smtp.live.com")
                          {
                            name.IsChoosen = true;
                          }
                          else
                          {
                            name.IsChoosen = false;
                          }
                        }
                        break;
                    //smtp.office365.com
                    case "office365.com":
                      foreach(SmtpServerName name in smtpServersNames)
                      {
                        if(name.Name == "smtp.office365.com")
                        {
                          name.IsChoosen = true;
                        }
                        else
                        {
                          name.IsChoosen = false;
                        }
                      }
                      break;
                    //smtp.mail.yahoo.com
                    case "mail.yahoo.com":
                      foreach(SmtpServerName name in smtpServersNames)
                      {
                        if(name.Name == "smtp.mail.yahoo.com")
                        {
                          name.IsChoosen = true;
                        }
                        else
                        {
                          name.IsChoosen = false;
                        }
                      }
                      break;
                    //smtp.o2.ie
                    case "o2.ie":
                      foreach(SmtpServerName name in smtpServersNames)
                      {
                        if(name.Name == "smtp.o2.ie")
                        {
                          name.IsChoosen = true;
                        }
                        else
                        {
                          name.IsChoosen = false;
                        }
                      }
                      break;

                    default:
                      Console.WriteLine("I don't know that email provider. \n \r Please enter a custom port.");
                      break;
              }
              Console.WriteLine("Now, write the senders password:");
              ConsoleKeyInfo keyChar;
              string passwordInput = "";
              do
              {
                if(keyChar.Key != ConsoleKey.Backspace)
                {
                  passwordInput += keyChar.Key.ToString();
                  Console.Write("*");
                }
                else
                {
                  passwordInput = passwordInput.Substring(0,passwordInput.Length-1);
                  Console.Write("\b");
                }
              }
              while(keyChar.Key != ConsoleKey.Enter);
              Program.message.Sender.Password = passwordInput;
              break;

          }
        }

        internal void Show(string property)
        {
            switch (property)
            {
                case "ports":
                    foreach (SmtpServerName port in smtpServersNames)
                    {
                        if (port.IsChoosen == true)
                            Console.WriteLine("[*]" + port.Name);
                        else
                            Console.WriteLine("[ ]" + port.Name);
                    }
                    break;
                case "message":
                    if (Program.message.Sender != null && Program.message.Sender.Email == "")
                    {
                        Console.WriteLine("From:" + Program.message.Sender.Email);
                    }
                    else
                    {
                        Console.WriteLine("There is no sender of a message!! Edit a sender data of message");
                    }
                    if (Program.message.Receivers != null && Program.message.Receivers.Count>0)
                    {
                        Console.WriteLine("To: ");
                        foreach (string receiver in Program.message.Receivers)
                        {
                            Console.Write(receiver + "; ");
                        }
                    }
                    else
                    {
                      Console.WriteLine("To: <none>");
                    }
                    Console.WriteLine("Subject:" + Program.message.Subject);
                    Console.WriteLine("Content:" + Program.message.Body);

                    break;
            }
        }

        internal void PrintWorkingDirectory()
        {
          Console.WriteLine(Directory.GetCurrentDirectory());
        }
        /// <summary>
        /// internal void ListFiles(string searchPattern)
        /// </summary>
        /// <param name="searchPattern"></param>
        internal void ListFiles()
        {
          string currentDirectory = Directory.GetCurrentDirectory();
          string[] directories = Directory.GetDirectories(currentDirectory);
          string[] files =Directory.GetFiles(currentDirectory);
          string result = "";
          string resultD= "";
          foreach(string dir in directories)
          {
            string dir2 = dir.Substring(currentDirectory.Length+1);
            resultD += dir2;
            resultD += "/  ";
          }
          foreach(string file in files)
          {
            string file2 = file.Substring(currentDirectory.Length+1);
            result += file2;
            result += "  ";
          }
          Console.WriteLine(resultD+result);
        }

        internal void RemoveReceiversFromMessage(Message message)
        {
          int i = 0;
          if(message.Receivers.Count > 0)
          {
            foreach(string receiver in message.Receivers)
            {
              Console.WriteLine("["+i+"]"+receiver);
              i++;
            }
            Console.Write("choose receiver for deletation by index:");
            var RemoveIndex = Console.ReadLine();
            try
            {
              int ConvIndex = Convert.ToInt32(RemoveIndex);
              if(ConvIndex<=message.Receivers.Count-1 &&ConvIndex>=0)
              message.Receivers.RemoveAt(ConvIndex);
              else
              {
                Console.WriteLine("Wrong input. Please write a number within a range of receivers index.");
              }
            }
            catch(Exception e)
            {
              Console.WriteLine("Wrong input. Please write a number within a range of receivers index.");
                    Console.WriteLine(e.Message);
            }

          }

        }
        internal void GetMail(Message message)
        {
          ImapController.GetMessages(message);
        }
    }
}
