using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using LightMailTerminal.Models;

namespace LightMailTerminal.Controlers
{
  public static class CommandFilter
  {
    static string headCommand;
    static internal string pointer;
    static public string VerifyCommand(string CommandInput)
    {
      switch(CommandInput)
      {
        case "send":
            return "send";
        case "save":
            return "save";
        case "edit title":
            return "edit subject";
        case "edit subject":
            return "edit subject";
        case "edit body":
            return "edit body";
        case "edit content":
            return "edit body";
        case "edit message":
            return "edit body";
        case "edit receivers":
            return "add receivers";
        case "add receivers":
            return "add receivers";
        case "edit sender":
            return "edit sender";
        case "edit credentials":
            return "edit sender";
        case "show ports":
            return "show ports";
        case "show message":
            return "show message";
        case "ls":
            return "ls";
        case "pwd":
            return "pwd";
        case "exit":
           return "exit";
        case "":
            return "";
        case "gm":
            return "get mail";
        case "get mail":
            return "get mail";
        default :
            ExtractHeadCommand(CommandInput);
            return headCommand;
      }
    }
    static public void ExtractHeadCommand(string CommandInput)
    {
      int i;
      headCommand = "";
      for(i=0; i<CommandInput.Length; i++)
      {

        headCommand += CommandInput.Substring(i,1);
        if(i==1 && headCommand=="cd")
        {
          pointer = CommandInput.Substring(3);
          //return headCommand;
          break;
        }
        else if(i==7 && headCommand=="pointat")
          {
            pointer = CommandInput.Substring(i+2);
            //return headCommand;
            break;
          }
          else if(i==CommandInput.Length-1)
          {
            headCommand = "--";
            //return headCommand;
            break;
          }
      }
      //return headCommand;
    }
    static public List<string> ExtractDirectorySteps(string directory)
    {
      List<string> directorySteps = new List<string>();
      List<int> directorySlash = new List<int>();
      string checkChar;
      int i,j;
      for(i=0;i<directory.Length; i++)
      {
        checkChar = directory.Substring(i,i+1);
        if(checkChar=="/")
        directorySlash.Add(i);
      }
      if(directorySlash.Count==0)
      {
        directorySteps.Add(directory);
      }
      else
      {
        for(j=0; j<=directorySlash.Count; i++)
        {
          if(j==0)
          {
            directorySteps.Add(directory.Substring(0,directorySlash[0]));
          }
          else if(j!=0 && j<directorySlash.Count)
          {
            directorySteps.Add(directory.Substring(directorySlash[j-1]+1,directorySlash[j]));
          }
          else if(j==directorySlash.Count)
          {
            directorySteps.Add(directory.Substring(directorySlash[j]));
          }
        }
      }
      return directorySteps;
    }
  }
}
